//
// L'ordre des imports est importante
//
import { BookFormComponent } from './book/book-form/book-form.component';
import { BookDetailsComponent } from './book/book-details/book-details.component';
import { BookComponent } from './book/book.component';
//
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// et ce sont les Routes
const routes: Routes = [
  // ça c'est le "default"
  { path: '', redirectTo: 'books', pathMatch: 'full' },
  // single book
  { path: 'book/:id', component: BookDetailsComponent },
  // create
  { path: 'add', component: BookFormComponent },
  // all the books
  { path: 'books', component: BookComponent },
  // update book
  { path: 'update', component: BookComponent }
  //
];
//
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
//
export class AppRoutingModule { }
//