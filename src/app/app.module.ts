import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
//
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//
import { BookComponent } from './book/book.component';
import { BookDetailsComponent } from './book/book-details/book-details.component';
import { BookFormComponent } from './book/book-form/book-form.component';


@NgModule({
  declarations: [
    AppComponent,
    //
    BookComponent,
    BookDetailsComponent,
    BookFormComponent
    //
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //
    HttpClientModule,
    FormsModule
    //
  ],
  //
  providers: [],
  bootstrap: [AppComponent]
  //
})
export class AppModule { }
