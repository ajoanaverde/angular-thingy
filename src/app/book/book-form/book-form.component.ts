import { BookService } from './../book.service';
import { Book } from './../book.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {
  book: Book = {
    titre: '',
    contenue: ''
  }
  isSubmit = false;

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
  }

  saveBook() {
    const data = {
      titre: this.book.titre,
      contenue: this.book.contenue
    }

    this.bookService.createBook(data).subscribe(result => {
      this.isSubmit = true;
    }, error => {
      console.log(error);
    });
  }

  newBook() {
    this.isSubmit = false;
    this.book = {
      titre: '', 
      contenue: ''
    }
  }

}