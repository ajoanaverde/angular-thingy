//
import { Component, OnInit } from '@angular/core';
// j'import le BookService q on a cree 
import { BookService } from './book.service';
//
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
//
// ça c'est ce q s'inicialise avec le server
export class BookComponent implements OnInit {
  books = [];
// le BookService q on a cree 
  constructor(private bookService: BookService) { }
// ça c'est comme un controller
  ngOnInit(): void {
    this.bookService.getBooks().subscribe(books => {
      console.log(books);
      this.books = books;
    })
  }

}


