export class Book {
// on construi la class , conforme à l'api
    constructor (
        public titre: string,
        public contenue: string,
    ) {}
}