import { Injectable } from '@angular/core';
//le model q j'ai fait
import { Book } from './book.model';
//
import { HttpClient } from '@angular/common/http';
//
@Injectable({
  providedIn: 'root'
})
export class BookService {
  // mettre ici l'adresse de l'autre server
  // on atribue l'adresse de l'api à une variable pour requeter dessus
  private REST_API_Server = 'http://localhost:3000/books';
  //
  // on declare une variable http pour l'atribuer l'httpclient
  constructor(private http: HttpClient) { }
  //
  // GETTER
  getBooks() {
    return this.http.get<Book[]>(this.REST_API_Server);
  }
  // GET BY ID
  getBookById(id: string) {
    return this.http.get<Book>(this.REST_API_Server + id)
  }
  // CREATE
  createBook(data: Book) {
    return this.http.post<Book>(this.REST_API_Server, data);
  }
  // DELETE
  deleteBookById(id: string) {
    this.http.delete<Book>(this.REST_API_Server + id);
  }
  // UPDATE
  // a finir
  // j'ai pas encore testé, je sais pas si ça marche
  updateBookById(id: string, data: Book) {
    return this.http.put<Book>(this.REST_API_Server + id, data);
  }
}
